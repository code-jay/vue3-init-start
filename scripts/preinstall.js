if (!/pnpm/.test(process.env.npm_execpath || '')) {
  console.warn(
    `\u001b[33mThis repository must using pnpm as the package manager ` +
    `for scripts to work properly. \u001b[39m\n` +
    `该项目必须使用pnpm`
  )
  process.exit(1)
}