
# vue3-init-start
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

    feat：    |  新功能（feature）
    fix：     |  修补bug
    docs：    |  文档（documentation）
    style：   |  格式（不影响代码运行的变动）
    refactor：|  重构（即不是新增功能，也不是修改bug的代码变动）
    perf：    |  优化，比如提升性能，体验
    test：    |  增加测试
    chore：   |  杂活，其他，构建过程或辅助工具的变动，依赖库，工具增加等    
    revert：  |  commit 回退
    build：   |  编译相关，发布版本，对项目构建或依赖的改动


## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) to make the TypeScript language service aware of `.vue` types.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
pnpm install
```

### Compile and Hot-Reload for Local Development

```sh
pnpm dev:test
```

### Type-Check, Compile and Minify for Development

```sh
pnpm build:dev
```

### Type-Check, Compile and Minify for Production

```sh
pnpm build:prod
```

### Lint with [ESLint](https://eslint.org/)

```sh
pnpm lint
```

### More

1. Pinia
2. Router
3. CommitLint
4. Husky 9
5. Prettier
6. Mandatory pnpm
7. Global components SvgIcon
8. element-plus
9. vite-plugin-mock
10. axios - Wrapped request.ts
11. vueDevTools - Need to uncomment : vite.config.ts