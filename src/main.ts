import { createApp } from "vue"
import { createPinia } from "pinia"

import App from "./App.vue"
import router from "./router"

import ElementPlus from "element-plus"
import "element-plus/dist/index.css"
import zhCn from "element-plus/es/locale/lang/zh-cn"

const app = createApp(App)
document.title = import.meta.env.VITE_APP_TITLE

app.use(createPinia())
app.use(router)
app.use(ElementPlus, {
  locale: zhCn
})
// svg插件配置代码
import "virtual:svg-icons-register"
// 引入自定义插件：注册全局组件
import globalComponent from "@/components"
app.use(globalComponent)
app.mount("#app")
