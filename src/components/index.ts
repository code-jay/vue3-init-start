import type { App, Plugin } from "vue"
import SvgIcon from "@/components/SvgIcon.vue"

const allGlobalComponent: Record<string, object> = { SvgIcon }

const MyPlugin: Plugin = {
  install(app: App) {
    Object.keys(allGlobalComponent).forEach((key) => {
      app.component(key, allGlobalComponent[key])
    })
  }
}

export default MyPlugin
