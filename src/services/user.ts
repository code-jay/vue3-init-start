import request from "@/services/request"

import type { loginFormData, loginResponseData, userInfoResponseData } from "@/types/user"

enum API {
  LOGIN_URL = "/admin/acl/index/login",
  USERINFO_URL = "/admin/acl/index/info",
  LOGOUT_URL = "/admin/acl/index/logout"
}

export const reqLogin = (data: loginFormData) => request.post<loginFormData, loginResponseData>(API.LOGIN_URL, data)

export const reqUserInfo = () => request.get<null, userInfoResponseData>(API.USERINFO_URL)

export const reqLogout = () => request.post<null, null>(API.LOGOUT_URL)
